import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Observable } from 'rxjs';
import { IonList, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {

  @ViewChild('lista') lista: IonList; 

  usuarios: Observable<any>;
  
  constructor( 
      private dataService: DataService,
      private toastCtrl: ToastController
  ) { }

  ngOnInit() {

  this.usuarios = this.dataService.getUsers();
  }

  async presentToast( msj: string) {
    const toast = await this.toastCtrl.create({
      message: msj,
      duration: 2000
    });
    toast.present();
  }

  borrar(item){
    this.presentToast('Eliminado');
    this.lista.closeSlidingItems();
  }

  favorite(item){
    this.presentToast('Favorito');
    this.lista.closeSlidingItems();
  }

  share(item){
    this.presentToast('Compartido');
    this.lista.closeSlidingItems();
  }



}
